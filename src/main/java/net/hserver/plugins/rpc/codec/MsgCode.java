package net.hserver.plugins.rpc.codec;

public enum MsgCode {
    SUCCESS,
    ERROR;

    private MsgCode() {
    }
}
