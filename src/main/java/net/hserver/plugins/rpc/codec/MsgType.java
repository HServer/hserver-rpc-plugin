package net.hserver.plugins.rpc.codec;

public enum MsgType {
    INVOKER,
    RESULT,
    HEART;

    private MsgType() {
    }
}
