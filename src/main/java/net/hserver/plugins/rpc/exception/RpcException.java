package net.hserver.plugins.rpc.exception;

public class RpcException extends RuntimeException {

    public RpcException(String message) {
        super(message);
    }
}
